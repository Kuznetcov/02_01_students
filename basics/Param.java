package basics;

import com.beust.jcommander.Parameter;

/**
 * параметры JCommander
 *
 * @author Nold
 */
public class Param {

  @Parameter(names = {"-help", "-?"}, help = true)
  private boolean help;

  public boolean isHelp() {
    return help;
  }

  @Parameter(names = {"-name"}, description = "vмя студента ")
  private String name;

  @Parameter(names = {"-sort"}, description = "Tип сортировки dывода студентов.")
  private String sortType = "resavg";

  public String getName() {
    return name;
  }

  public String getSortType() {
    return sortType;
  }
}
