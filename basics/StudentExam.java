package basics;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Random;

class StudentExam {

  public static void exam(Student stud, long days) {
    Random r = new Random();
    for (int i = 0; i < days; i++) {
      stud.addMark(2 + r.nextInt(4));
    }
  }

  public static float averageMark(ArrayList<Integer> marks) {
    float average = 0;
    for (int i = 0; i < marks.size(); i++) {
      average += marks.get(i);
    }
    average = average / marks.size();
    return (average);
  }

  public static void future(Student stud, LocalDate date) {
    ArrayList<Integer> marks = new ArrayList<Integer>(stud.getMarks());
    for (long i = 0; i < ChronoUnit.DAYS.between(date, stud.getFinishDate()); i++) {
      marks.add(5);
    }
    if (averageMark(marks) < 4.5) {
      stud.setStudentFuture("ќтчислен");
    } else stud.setStudentFuture("∆ить будет");
  }
}
