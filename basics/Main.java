package basics;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

public class Main {

  public static void main(String[] args) {
    Param comLine = new Param();
    JCommander jc = new JCommander(comLine);
    // JCommander - ииспользуется для работы с командной строкой

    String[] argv = {"-sort", "resavg"};
    String date = "25.04.16";

    try { // проверка параметров
      jc.parse(argv);

      if (comLine.isHelp()) {
        jc.usage();
        System.exit(0);
      }
      if (comLine.getName() != null) {
        System.out.println("Выводим dанные по студенту: " + comLine.getName());
      } else {
        System.out.println("Выводим cписок студентов." + ("time".equals(comLine.getSortType())
            ? " Сортировка по oставшемуся времени" : " Сортировка по среднему баллу"));
      }

    } catch (ParameterException t) {
      System.out.println("Параметры dведены неправильно. Для справки используйте -help");
      System.exit(0);
    }

    ArrayList<Student> students = new ArrayList<Student>();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yy");
    LocalDate actualDate = LocalDate.parse(date, formatter);


    students.add(new Student("Stud Odin", Curcillum.getRandomCurcillum(),
        LocalDate.parse("21.04.16", formatter)));
    students.add(new Student("Stud Dva", Curcillum.getRandomCurcillum(),
        LocalDate.parse("22.04.16", formatter)));
    students.add(new Student("Stud Tri", Curcillum.getRandomCurcillum(),
        LocalDate.parse("23.04.16", formatter)));
    students.add(new Student("Stud Chet", Curcillum.getRandomCurcillum(),
        LocalDate.parse("24.04.16", formatter)));

    for (int i = 0; i < students.size(); i++) {
      StudentExam.exam(students.get(i),
          ChronoUnit.DAYS.between(students.get(i).getStartDate(), actualDate));
      StudentExam.future(students.get(i), actualDate);
    }

    if (comLine.getName() != null) {
      for (int i = 0; i < students.size(); i++) {
        if ((students.get(i).getFio()).equals(comLine.getName())) {
          Student ourStudent = students.get(i);
          System.out.println("STUDENT: " + ourStudent.getFio() + "\n" + "CURCILLUM: "
              + ourStudent.getCurcillum() + "\n" + "START_DATE: " + ourStudent.getStartDate() + "\n"
              + Curcillum.valueOf(ourStudent.getCurcillum()).getCurcInfo() + " " + "MARKS: "
              + ourStudent.getMarks());
        }
      }

    } else if (comLine.getSortType().equals("resavg")) {

      Comparator<Student> c = (s1, s2) -> s1.getAverageMark().compareTo(s2.getAverageMark());
      students.sort(c);
      students.forEach(stud -> System.out.printf(
          "%s - До окончания обучения по программе %s осталось %d дней. Сrедний балл: %.2f \n %s %s \n\n",
          stud.getFio(), stud.getCurcillum(),
          ChronoUnit.DAYS.between(actualDate, stud.getFinishDate()), stud.getAverageMark(),
          stud.getStudentFuture(), stud.getMarks()));

    } else {

      Comparator<Student> c = (s1, s2) -> s1.getFinishDate().compareTo(s2.getFinishDate());
      students.sort(c);
      students.forEach(stud -> System.out.printf(
          "%s - До окончания обучения по программе %s осталось %d дней. Сrедний балл: %.2f \n %s %s \n\n",
          stud.getFio(), stud.getCurcillum(),
          ChronoUnit.DAYS.between(actualDate, stud.getFinishDate()), stud.getAverageMark(),
          stud.getStudentFuture(), stud.getMarks()));
    }
  }
}
