package basics;

import java.util.Random;

public enum Curcillum {

  J2EEDEVELOPER("info placeholder", 48),

  JAVADEVELOPER("info placeholder", 48),

  CDEVELOPER("info placeholder", 48),

  WEBDEVELOPER("info placeholder", 48);

  private Integer duration;
  private String curcInfo;

  Curcillum(String curcInfo, Integer duration) {
    this.duration = duration;
    this.curcInfo = curcInfo;

  };

  public static String getRandomCurcillum() {
    Random r = new Random();
    Curcillum[] types = Curcillum.values();
    Curcillum type = types[r.nextInt(Curcillum.values().length)];
    return (type.toString());
  }

  public Integer getDuration() {
    return duration;
  }

  public Integer getDurationDays() {
    return (duration / 8);
  }

  protected void setDuration(Integer duration) {
    this.duration = duration;
  }

  public String getCurcInfo() {
    return curcInfo;
  }

  protected void setCurcInfo(String curcInfo) {
    this.curcInfo = curcInfo;
  }
}
