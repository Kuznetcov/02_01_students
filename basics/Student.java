package basics;

import java.time.LocalDate;
import java.util.ArrayList;

public class Student {

  private String fio;
  private String curcillum;
  private LocalDate startDate;
  private LocalDate finishDate;
  private String studentFuture = "∆ить будет";
  private ArrayList<Integer> marks = new ArrayList<Integer>();

  public Student(String name, String curs, LocalDate date) {
    this.setFio(name);
    this.setCurcillum(curs);
    this.setStartDate(date);
    this.setFinishDate(date, curs);
  }

  private void setFinishDate(LocalDate date, String curs) {
    this.finishDate = date.plusDays(Curcillum.valueOf(curs).getDurationDays());
  }

  public String getFio() {
    return fio;
  }

  protected void setFio(String fio) {
    this.fio = fio;
  }

  public String getCurcillum() {
    return curcillum;
  }

  protected void setCurcillum(String curcillum) {
    this.curcillum = curcillum;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  protected void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }

  public LocalDate getFinishDate() {
    return finishDate;
  }

  public void addMark(int mark) {
    this.marks.add(mark);
  }

  public ArrayList<Integer> getMarks() {
    return marks;
  }

  public String getStudentFuture() {
    return studentFuture;
  }

  public void setStudentFuture(String studentFuture) {
    this.studentFuture = studentFuture;
  }
  
  public Float getAverageMark(){
    return StudentExam.averageMark(this.marks);
  }
}
